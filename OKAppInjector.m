//
//  OKAppInjector.m
//  Odnoklassniki
//
//  Created by georgy.kasapidi on 29.12.16.
//  Copyright © 2016 Odnoklassniki. All rights reserved.
//

#import <objc/runtime.h>

#import "OKAppInjector.h"

@interface OKAppInjector ()

@property (strong, nonatomic) NSHashTable<id<OKInjection>> *injections;
@property (strong, nonatomic) NSHashTable<id<OKInjectableObject>> *injectables;

@property (copy, nonatomic) NSString *markAsInjectionProtoName;

@end

@implementation OKAppInjector

ok_injection_implementation(OKAppInjectorUsage, setInjector:)

- (instancetype)init {
    self = [super init];

    if (self != nil) {
        _injections = [NSHashTable weakObjectsHashTable];
        _injectables = [NSHashTable weakObjectsHashTable];

        _markAsInjectionProtoName = NSStringFromProtocol(@protocol(OKMarkAsInjection));
    }

    return self;
}

#pragma mark -

- (void)registerInjection:(id<OKInjection>)injection {
    NSParameterAssert([injection conformsToProtocol:@protocol(OKInjection)]);

    @synchronized (self) {
        if (injection == self) {
            return;
        }

        if ([self.injections containsObject:injection]) {
            return;
        }

        [[self.injectables allObjects] enumerateObjectsUsingBlock:^(id<OKInjectableObject>  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([injection injectSelfTo:obj]) {
                [self __notifyInjectableObject:obj];
            }
        }];

        [self.injections addObject:injection];
    }
}

- (void)registerInjectableObject:(id<OKInjectableObject>)injectableObject {
    NSParameterAssert([injectableObject conformsToProtocol:@protocol(OKInjectableObject)]);

    @synchronized (self) {
        if ([self.injectables containsObject:injectableObject]) {
            return;
        }

        [self injectSelfTo:injectableObject];
        [[self.injections allObjects] makeObjectsPerformSelector:@selector(injectSelfTo:) withObject:injectableObject];

        [self __notifyInjectableObject:injectableObject];

        [self.injectables addObject:injectableObject];
    }
}

#pragma mark -

- (void)__notifyInjectableObject:(id<OKInjectableObject>)obj {
    if ([obj respondsToSelector:@selector(dependenciesWereInjectedByInjector:)] &&
        [self __dependenciesWereInjectedToObject:obj]) {

        [obj dependenciesWereInjectedByInjector:self];
    }
}

- (BOOL)__dependenciesWereInjectedToObject:(id)obj {
    Class cls = [obj class];

    while (cls != Nil && cls != NSObject.class) {
        uint propCount = 0;
        objc_property_t *props = class_copyPropertyList(cls, &propCount);

        for (uint i = 0; i < propCount; ++i) {
            objc_property_t prop = props[i];

            NSString *propAttrs = [NSString stringWithUTF8String:property_getAttributes(prop)];

            if ([propAttrs rangeOfString:self.markAsInjectionProtoName].location != NSNotFound) {
                NSString *propName = [NSString stringWithUTF8String:property_getName(prop)];

                if (![obj valueForKey:propName]) {
                    free(props);
                    
                    return NO;
                }
            }
        }
        
        free(props);

        cls = class_getSuperclass(cls);
    }

    return YES;
}

@end
