//
//  OKInjectionExtensions.h
//  Odnoklassniki
//
//  Created by georgy.kasapidi on 13.01.17.
//  Copyright © 2017 Odnoklassniki. All rights reserved.
//

#import <OKBaseKit/OKAppInjector.h>

ok_injection_class_usage(OKUIService, uiService)
ok_injection_class_usage(OKAPIService, apiService)
ok_injection_class_usage(OKWMFClient, wmfClient)
ok_injection_class_usage(OKApiEventsService, apiEventsService)
ok_injection_class_usage(OKApiSettingsService, apiSettingsService)
ok_injection_class_usage(OKDBManager, dbManager)
ok_injection_class_usage(OKChatListService, chatListService)
ok_injection_class_usage(OKChatUpdateQueue, chatUpdateQueue)
ok_injection_class_usage(OKVideoChatController, videoChatController)
ok_injection_class_usage(OKStatusBarNotificationService, statusBarNotificationService)
ok_injection_class_usage(OKPlayerContainer, playerContainer)
ok_injection_class_usage(OKPresentsShowcaseRouter, presentsRouter)
ok_injection_class_usage(OKAudioRouter, musicRouter)
ok_injection_class_usage(OKAppEnvironment, appEnvironment)
ok_injection_class_usage(YYWebImageManager, imageManager)
ok_injection_class_usage(OKAudioController, audioController)
ok_injection_class_usage(OKAppBannersProvider, appBannersProvider)
ok_injection_class_usage(OKLocalizationService, localizationService)
ok_injection_class_usage(OKFriendsService, friendsService)
ok_injection_class_usage(OKMRServicesProvider, mrServiceProvider)
ok_injection_class_usage(OKAVAudioSessionManager, audioSessionManager)
