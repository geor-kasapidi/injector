//
//  OKInjectionExtensions.m
//  Odnoklassniki
//
//  Created by georgy.kasapidi on 13.01.17.
//  Copyright © 2017 Odnoklassniki. All rights reserved.
//

#import "OKInjectionExtensions.h"

#import <YYWebImage/YYWebImage.h>
#import <OKDataBase/OKDBManager.h>
#import <OKMusicKit/OKAudioController.h>
#import <OKMusicKit/OKWMFClient.h>

#import "OKUIService.h"
#import "OKAPIService.h"
#import "OKApiEventsService.h"
#import "OKApiSettingsService.h"
#import "OKChatListService.h"
#import "OKChatUpdateQueue.h"
#import "OKVideoChatController.h"
#import "OKStatusBarNotificationService.h"
#import "OKPlayerContainer.h"
#import "OKPresentsShowcaseRouter.h"
#import "OKAudioRouter.h"
#import "OKAppEnvironment.h"
#import "OKAppBannersProvider.h"
#import "OKLocalizationService.h"
#import "OKFriendsService.h"
#import "OKMRServicesProvider.h"
#import "OKAVAudioSessionManager.h"

ok_injection_implementation_extension(OKUIService, OKUIServiceUsage, setUiService:)
ok_injection_implementation_extension(OKAPIService, OKAPIServiceUsage, setApiService:)
ok_injection_implementation_extension(OKWMFClient, OKWMFClientUsage, setWmfClient:)
ok_injection_implementation_extension(OKApiEventsService, OKApiEventsServiceUsage, setApiEventsService:)
ok_injection_implementation_extension(OKApiSettingsService, OKApiSettingsServiceUsage, setApiSettingsService:)
ok_injection_implementation_extension(OKDBManager, OKDBManagerUsage, setDbManager:)
ok_injection_implementation_extension(OKChatListService, OKChatListServiceUsage, setChatListService:)
ok_injection_implementation_extension(OKChatUpdateQueue, OKChatUpdateQueueUsage, setChatUpdateQueue:)
ok_injection_implementation_extension(OKVideoChatController, OKVideoChatControllerUsage, setVideoChatController:)
ok_injection_implementation_extension(OKStatusBarNotificationService, OKStatusBarNotificationServiceUsage, setStatusBarNotificationService:)
ok_injection_implementation_extension(OKPlayerContainer, OKPlayerContainerUsage, setPlayerContainer:)
ok_injection_implementation_extension(OKPresentsShowcaseRouter, OKPresentsShowcaseRouterUsage, setPresentsRouter:)
ok_injection_implementation_extension(OKAudioRouter, OKAudioRouterUsage, setMusicRouter:)
ok_injection_implementation_extension(OKAppEnvironment, OKAppEnvironmentUsage, setAppEnvironment:)
ok_injection_implementation_extension(YYWebImageManager, YYWebImageManagerUsage, setImageManager:)
ok_injection_implementation_extension(OKAudioController, OKAudioControllerUsage, setAudioController:)
ok_injection_implementation_extension(OKAppBannersProvider, OKAppBannersProviderUsage, setAppBannersProvider:)
ok_injection_implementation_extension(OKLocalizationService, OKLocalizationServiceUsage, setLocalizationService:)
ok_injection_implementation_extension(OKFriendsService, OKFriendsServiceUsage, setFriendsService:)
ok_injection_implementation_extension(OKMRServicesProvider, OKMRServicesProviderUsage, setMrServiceProvider:)
ok_injection_implementation_extension(OKAVAudioSessionManager, OKAVAudioSessionManagerUsage, setAudioSessionManager:)
