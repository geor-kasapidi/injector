//
//  OKAppInjector.h
//  Odnoklassniki
//
//  Created by georgy.kasapidi on 29.12.16.
//  Copyright © 2016 Odnoklassniki. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OKAppInjector;

@protocol OKInjectableObject <NSObject>

@optional;
- (void)dependenciesWereInjectedByInjector:(id<OKAppInjector>)injector;

@end

@protocol OKInjection <NSObject>

- (BOOL)injectSelfTo:(id<OKInjectableObject>)injectableObject;

@end

#pragma mark - OKAppInjector

@protocol OKAppInjector <NSObject>

- (void)registerInjection:(id)injection; // id<OKInjection>
- (void)registerInjectableObject:(id)injectableObject; // id<OKInjectableObject>

@end

@interface OKAppInjector : NSObject <OKAppInjector, OKInjection>
@end

#define ok_injection_implementation(proto, sel) \
        - (BOOL)injectSelfTo:(id<OKInjectableObject>)injectableObject { \
            if ([injectableObject conformsToProtocol:@protocol(proto)]) { \
                [(id<proto>)injectableObject performSelector:@selector(sel) withObject:self]; \
                return YES; \
            } \
            return NO; \
        }

#define ok_injection_implementation_extension(cls, proto, setter) \
        @interface cls (Injection) <OKInjection> \
        @end \
        @implementation cls (Injection) \
        ok_injection_implementation(proto, setter) \
        @end

@protocol OKMarkAsInjection <NSObject>
@end

#define ok_injection_class_usage(cls, prop) \
        @class cls; \
        @protocol cls##Usage <OKInjectableObject> \
        @property (weak, nonatomic) cls<OKMarkAsInjection> *prop; \
        @end

ok_injection_class_usage(OKAppInjector, injector)
